<?php
class secretSanta {
	public $people = array(
		'henk' => array(),
		'sjoerd' => array(),
		'klaas' => array(),
		'piet' => array()
	);

	public function main() {
		$this->fillArray();

		$this->pickSecretSanta();

		$this->printResult();
	}


	public function fillArray() {
		foreach ($this->people as $masterkey => $mastervalue) {
			$array = array(
				'candidates' => array(),
				'picked' => false
			);

			foreach ($this->people as $key => $value) {
				if (!strcmp($masterkey, $key) == 0) {
					$array['candidates'][$key] = false;
				}
			}
		 $this->people[$masterkey] = $array;
		
		}
	}

	public function pickSecretSanta() {
		foreach ($this->people as $key => $value) {
			$this->matchPeople($key, $value);
		}
	}

	public function matchPeople($key, $value) {
		$reciver = array_rand($value['candidates']);

		if (!$this->people[$reciver]['picked']) {
			$this->people[$key]['candidates'][$reciver] = true;
			$this->people[$reciver]['picked'] = true;
		} else {
			$this->matchPeople($key, $value);
		}
	}

	public function printResult() {
		foreach ($this->people as $key => $value) {
			$reciver = array_search(true, $value['candidates']);
			echo $key . ' gives a gift to ' . $reciver . "\n";
		}
		
	}
}
